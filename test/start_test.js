jasmine.DEFAULT_TIMEOUT_INTERVAL = 1200000

const path = require('path')
// require('app-module-path').addPath(path.join(__dirname, '..', '..'))

const logger = require('@pubsweet/logger')

const fixtures = require('./fixtures')
const dbconfig = fixtures.dbconfig
const flow = require('./helpers/flow')
const pubsweet = require('./helpers/pubsweet')

describe('start', () => {
  let server

  beforeAll(async (done) => {
    try {
      await require('../src/scripts/generate-env')()

      await require('../src/scripts/newdb')({
        properties: require('../src/scripts/db-properties'),
        override: dbconfig
      })

      logger.info('Starting server')
      require('../src/scripts/start')(_server => {
        server = _server
        logger.info('Server started')
        done()
      })
    } catch (e) {
      done.fail(e)
    }
  })

  afterAll(done => {
    logger.info('Stopping the server')
    server.close(() => {
      logger.info('Server stopped')
      done()
    })
  })

  // FLOW:
  // - start the app
  // - visit the login page
  // - login as the admin user
  // - visit the homepage

  it('should allow admin to log in', async () => {
    const promise = flow()
      .use(pubsweet.login(dbconfig))
      .waitForUrl(/manage.posts/)
      .wait('nav')
      .evaluate(() => document.querySelector('h2').innerText)
      .end()

    await expect(promise).resolves.toBe(dbconfig.collection)
  })
})