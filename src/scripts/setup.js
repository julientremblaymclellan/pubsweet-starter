const logger = require('@pubsweet/logger')
const colors = require('colors/safe')
const program = require('commander')
const properties = require('../src/scripts/db-properties')

module.exports = async args => {
  program
    .arguments('[path]')
    .description('Setup a database for this PubSweet app')
    .option('--clobber', 'Overwrite any existing database')

  Object.keys(properties).forEach(key => {
    program.option(`--${key} [string]`, properties[key].description)
  })

  program.parse(args || process.argv)

  logger.info('Generating PubSweet database')

  await require('../src/scripts/newdb')({
    appPath: appPath,
    properties: require('../src/scripts/db-properties'),
    override: program
  })
}

